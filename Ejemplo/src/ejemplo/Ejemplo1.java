

package ejemplo;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Ejemplo1 {

   public static int x = 999;
    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Ejemplo1().run();
            }
        });

    }

    
    void run() {
        Hilo h1 = new Hilo("H1", 10,1000,this);
        Hilo h2 = new Hilo("H2", 15,500,this);
        Hilo h3 = new Hilo("H3", 20,1500,this);

        h1.start();
        
         try {
                h1.join();
            } catch (InterruptedException ex) {
                Logger.getLogger(Ejemplo1.class.getName()).log(Level.SEVERE, null, ex);
            }
        h2.start();
        h3.start();

        for (int c= 0; c < 50; c++) {
            System.out.printf("Contador principal c = %d\n",c);
             
    }

}

class Hilo extends Thread {

    int n=100;
    String id="";
    int t;
    Ejemplo1 este;

    public Hilo(String _id,int _n,int _t,Ejemplo1 _este) {
        this.id = _id;
        this.n=_n;
        this .t=_t;
        this.este= _este;
    }

}
}
 

 
