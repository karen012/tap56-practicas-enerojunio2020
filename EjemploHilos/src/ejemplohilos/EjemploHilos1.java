package ejemplohilos;

import java.util.logging.Level;
import java.util.logging.Logger;

public class EjemploHilos1 {

    int x = 0;
    Object semaforo = new Object();

    public static void main(String[] args) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new EjemploHilos1().ejecutar();
            }
        });
    }

    private void ejecutar() {

        Hilo h1 = new Hilo("h1", 10, 1000);
        Hilo h2 = new Hilo("h2", 15, 1500);
        Hilo h3 = new Hilo("h3", 20, 500);

        h1.start();
        h2.start();
        h3.start();

        for (int i = 0; i < 30; i++) {
            System.out.printf("Proceso Principal : x = %d\n ", i);

            synchronized (semaforo) {
                this.x = i;
            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(EjemploHilos1.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

    }

    class Hilo extends Thread {

        int n = 0;
        int t = 0;
        String id = "";

        public Hilo(String _id, int _n, int _t) {
            this.n = _n;
            this.id = _id;
            this.t = _t;

        }

        @Override
        public void run() {
            for (int i = 0; i < this.n; i++) {

                synchronized (semaforo) {
                    System.out.printf("%s : i = %d,x=%d \n", this.id, i, x);
                }

                try {
                    Thread.sleep(this.t);
                } catch (InterruptedException ex) {
                    Logger.getLogger(EjemploHilos1.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
    

    }
}
        
    

