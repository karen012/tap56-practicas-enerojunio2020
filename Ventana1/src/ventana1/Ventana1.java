package ventana1;

import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.*;


class Ventana1 extends JFrame implements ActionListener {
    
    JLabel etiq1;
    JTextField nombre;
    JButton btn;                   
    
    Ventana1(){
       this.setTitle("Practica 1");
       this.setSize(300,150);
       this.setLayout(new FlowLayout());
       
       this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

       etiq1 = new JLabel("Escriba un nombre para saludar");
       nombre = new JTextField(20);
       btn = new JButton("¡Saludar¡"); 
       
       btn.addActionListener(this);
        
       this.add(etiq1);
       this.add(nombre);
       this.add(btn);       
    }
           
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {                   
                new Ventana1().setVisible(true);
            }
        });        
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JOptionPane.showMessageDialog(this, "Hola! "+this.nombre.getText());
    }
    
}
