
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Servidor {

    PrintWriter out = null;
    BufferedReader in = null;
    BufferedReader stdIn = null;

    public static void main(String args[]) {
        Servidor srv = new Servidor();
        srv.ejecutar();
    }

    public void ejecutar() {
        ServerSocket servidor;
        Socket conexion;
        String respuesta = "";

        stdIn = new BufferedReader(new InputStreamReader(System.in));

        try {
            servidor = new ServerSocket(1001);

            while (true) {

                conexion = servidor.accept();

                System.out.printf("Se acepto una conexion desde: %s\n", conexion.getInetAddress());

                out = new PrintWriter(conexion.getOutputStream(), true);

                in = new BufferedReader(
                        new InputStreamReader(conexion.getInputStream()));

                out.println("Bueno");
                System.out.printf("Le dijimos a este, %s 'Bueno\n", conexion.getInetAddress());

                respuesta = in.readLine();

                System.out.printf("Esta fue la respuesta: %s \n", respuesta);

                conexion.close();
            }

        } catch (IOException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
